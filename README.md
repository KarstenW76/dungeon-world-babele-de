# dungeon-world-babele-de

German translation of Asocalips amazing Dungeon World system for Foundry VTT, translated with the help of Simones module.

Asocalips hat ein großartiges System für Dungeon World in Foundry VTT erstellt: https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld
In diesem findet Ihr bereits einen deutsch übersetzten Charakterbogen vor.

Mit Hilfe von Simones Modul Babele (https://gitlab.com/riccisi/foundryvtt-babele) konnte ich realisieren, dass Ihr das bereitgestellte Kompendium ebenso in deutsch angezeigt bekommt (statt Englisch), aus lizenzrechtlichen Gründen in Abstimmung mit System Matters gilt das für die Inhalte, die Ihr auch im deutschen Referenzdokument findet.

Die Freigabe von System Matters umfasst fast alle Gegenstände, die Spielzüge und Klassen, sowie einige Basisinfos und Nachschlageseiten (insbesondere eine Übersicht der Spielzüge und Kosten für Services).
Keine Übersetzung aus Lizenzgründen findet Ihr zu den magischen Gegenstände, den Monstern und der Klasse "The Immolator". Wenn Ihr diese in Englisch ausgeblendet haben möchtet, hierfür nutze ich persönlich das Modul "Compendium Hider" (https://github.com/earlSt1/vtt-compendium-hider), womit man nicht benötigte Kompendien auch für sich selbst als GM ausblenden kann (mit der Sichtbarkeitseinstellung tut man das sonst ja nur für die Spieler im Browser).

<h3>Installation</h3>
1. Foundry VTT starten und Hauptmenü aufrufen
2. "Add-on Module" aufrufen und auf <Modul installieren> klicken, dann "German [Core]" suchen und installieren. Alternativ direkt das Manifest "https://gitlab.com/henry4k/foundryvtt-lang-de/-/raw/master/module.json" installieren.
3. "Spielsysteme" aufrufen und auf <System installieren> klicken, dann "Dungeon World" suchen und installieren. Alternativ direkt das Manifest "https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld/raw/master/system.json" installieren.
4. "Add-on Module" aufrufen und auf <Modul installieren> klicken, dann "Babele" suchen und installieren. Alternativ direkt das Manifest "https://gitlab.com/riccisi/foundryvtt-babele/raw/master/module/module.json" installieren.
5. "Add-on Module" aufrufen und auf <Modul installieren> klicken, dann das Manifest "https://gitlab.com/KarstenW76/dungeon-world-babele-de/-/raw/master/module.json" installieren.
6. Welt für Dungeon World erstellen bzw. falls schon vorhanden laden; Einstellungen anpassen -> Grundeinstellungen -> Sprache auf "Deutsch" einstellen.
7. Spieleinstellungen -> Manage Modules: "Babele" und "Dungeon World Babele German" aktivieren.
8. Viel Spaß bei Dungeon World in deutsch!
Wenn Ihr Übersetzungsfehler findet gerne melden, dann schau ich mir das an.

<h3>Pfade</h3>
- Repo: https://gitlab.com/KarstenW76/dungeon-world-babele-de/
- Manifest: https://gitlab.com/KarstenW76/dungeon-world-babele-de/-/raw/master/module.json
- Download: https://gitlab.com/KarstenW76/dungeon-world-babele-de/-/archive/master/dungeon-world-babele-de-master.zip

<h3>Note of thanks</h3>
Many thanks to Asocalips for the system creation and to Simone for the Babele integration! You did and still do amazing work!
Und vielen Dank an Daniel und Stefan von System Matters (https://www.system-matters.de) für die Genehmigung! Ihr macht tolle Spiele und Übersetzungen, danke dafür!