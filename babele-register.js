
Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {
		
		Babele.get().register({
			module: 'dungeon-world-babele-de',
			lang: 'de',
			dir: 'compendium'
		});
	}
});